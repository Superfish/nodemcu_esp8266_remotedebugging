/*
 *  This sketch sends a message to a TCP server
 *
 */

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <WiFiUDP.h>

ESP8266WiFiMulti WiFiMulti;

WiFiUDP udp;
WiFiUDP udpRecv;
int i;
String receiverIP;
IPAddress broadcastIp;


void setup() {
    i = 0;
    Serial.begin(115200);
    delay(10);

    // We start by connecting to a WiFi network
    WiFi.mode(WIFI_STA);
    WiFiMulti.addAP("inMusic_Corp", "1nMusicW!F!");

    Serial.println();
    Serial.println();
    Serial.print("Wait for WiFi... ");

    while(WiFiMulti.run() != WL_CONNECTED) {
        Serial.print(".");
        delay(500);
    }

    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
    broadcastIp = ~WiFi.subnetMask() | WiFi.gatewayIP();
    Serial.println(broadcastIp);

    delay(500);

    udpRecv.begin(11234);
}


void loop() {
    
    if(i++ % 500000 == 0)
    {
      //Serial.println("Sending keepalive");
      udp.beginPacket(broadcastIp, 11234); 
      udp.write(1);
      udp.endPacket();
    }
    if(receiverIP.length()>0){
      int n=0;
      char buffer[128];
      if (Serial.available() > 0) 
      {
        //Serial.println("Sending");
        //Serial.println(receiverIP.c_str());
        while(Serial.available() && n < 128)
        {
          buffer[n++] = Serial.read();
        }
        char *p = const_cast<char*>(receiverIP.c_str());
        udp.beginPacket(p, 11234);
        udp.write(2);
        udp.write(buffer, n);
        udp.endPacket();
      }
    }

    int packetSize = udpRecv.parsePacket();
    if(packetSize)
    {
      char incoming[128];
      int len = udpRecv.read(incoming, 128);
      receiverIP = udpRecv.remoteIP().toString();
      if(len > 0)
      {
        incoming[len] = 0;
      }
      Serial.print(incoming);
    }
}

