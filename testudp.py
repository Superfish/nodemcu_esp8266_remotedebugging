import socket,sys
import _thread
import getch
import tty, termios

UDP_IP = "0.0.0.0"
UDP_PORT = 11234
sendAddr = "10.15.0.255"

def receiver():
	global sendAddr
	sock = socket.socket(socket.AF_INET, # Internet
	                     socket.SOCK_DGRAM) # UDP

	sock.bind((UDP_IP, UDP_PORT))

	clients = {}

	while True:
		data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
		sendAddr = str(addr[0])
		#sys.stdout.write(str(data))
		#sys.stdout.flush()

		if len(data) == 1:
			if data[0] == 1:
				if addr in clients:
					#sys.stdout.write(".")
					#sys.stdout.flush()
					pass
				else:
					sys.stdout.write("+\n")
					sys.stdout.flush()
					sock.sendto(b'\0', (sendAddr, UDP_PORT))
					clients[addr] = ""
		elif data[0] == 2:
			sys.stdout.buffer.write(data[1:])
			sys.stdout.flush()


_thread.start_new_thread( receiver, () )

#receiver()

def getch():
	fd = sys.stdin.fileno()
	old_settings = termios.tcgetattr(fd)
	try:
		tty.setraw(sys.stdin.fileno())
		ch = sys.stdin.read(1)
	finally:
		termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
	return ch

def sender():
	global sendAddr
	sock = socket.socket(socket.AF_INET, # Internet
	                     socket.SOCK_DGRAM) # UDP

	while 1:
		if sendAddr != "10.15.0.255":
			#ch = getch.getch()
			
			ch = getch()
			sock.sendto(bytes(ch, 'utf-8'), (sendAddr, UDP_PORT))


sender()

