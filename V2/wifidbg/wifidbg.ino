/*
 *  This sketch sends a message to a TCP server
 *
 */

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <WiFiUDP.h>

#define MAX_SRV_CLIENTS 1

ESP8266WiFiMulti WiFiMulti;

WiFiUDP udp;
WiFiServer server(11234);
WiFiClient serverClients[MAX_SRV_CLIENTS];

int i;
String receiverIP;
IPAddress broadcastIp;

void setup() {
  i = 0;
  Serial.begin(115200);
  delay(10);
  
  // We start by connecting to a WiFi network
  WiFi.mode(WIFI_STA);
  WiFiMulti.addAP("inMusic_Corp", "1nMusicW!F!");
  
  Serial.println();
  Serial.println();
  Serial.print("Wait for WiFi... ");
  
  while(WiFiMulti.run() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  broadcastIp = ~WiFi.subnetMask() | WiFi.gatewayIP();
  Serial.println(broadcastIp);

  server.begin();
  server.setNoDelay(true);
}

void loop() {
  if(i++ % 500000 == 0)
  {
    //Serial.println("Sending keepalive");
    udp.beginPacket(broadcastIp, 11234); 
    udp.write("\x01TheDevice");
    udp.endPacket();
  }
  
  if (server.hasClient()) {
    for(i = 0; i < MAX_SRV_CLIENTS; i++) {
      //find free/disconnected spot
      if (!serverClients[i] || !serverClients[i].connected()) {
        if(serverClients[i]) 
          serverClients[i].stop();
        
        serverClients[i] = server.available();
        break;
      }
    }
    //no free/disconnected spot so reject
    if ( i == MAX_SRV_CLIENTS) {
       WiFiClient serverClient = server.available();
       serverClient.stop();
    }
  }
  //check clients for data
  for(i = 0; i < MAX_SRV_CLIENTS; i++){
    if (serverClients[i] && serverClients[i].connected()){
      if(serverClients[i].available()){
        //get data from the telnet client and push it to the UART
        while(serverClients[i].available())
          Serial.write(serverClients[i].read());
      }
    }
  }
  //check UART for data
  if(Serial.available()){
    size_t len = Serial.available();
    uint8_t sbuf[len];
    Serial.readBytes(sbuf, len);
    //push UART data to all connected telnet clients
    for(i = 0; i < MAX_SRV_CLIENTS; i++){
      if (serverClients[i] && serverClients[i].connected()){
        serverClients[i].write(sbuf, len);
      }
    }
  }
}

