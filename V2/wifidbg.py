#!/usr/bin/env python3 -u

import threading
import sys, os
import termios, tty
import socket, select
import queue
import time
import argparse

class InputThread (threading.Thread):
	def __init__(self, ev, queue):
		threading.Thread.__init__(self)
		self.ev = ev
		self.queue = queue

	def getch(self):
		fd = sys.stdin.fileno()
		old_settings = termios.tcgetattr(fd)
		try:
			tty.setraw(sys.stdin.fileno())
			ch = sys.stdin.read(1)
		finally:
			termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
		return ch

	def run(self):
		print ("Starting Input handler")
		print("\033[H\033[J")
		while True:
			data = self.getch()
			if ord(data) == 3: # Ctrl+C
				break
			self.queue.put(data)
			self.ev.set()
			
		print ("\nExiting Input handler")


class ReceiverThread (threading.Thread):
	def __init__(self, socket):
		threading.Thread.__init__(self)
		self.socket = socket
		self.stop = False

	def run(self):
		print ("Starting Receiver")
		while not self.stop:
			ready = select.select([self.socket], [], [], 1)
			if ready[0]:
				data = self.socket.recv(128)
				sys.stdout.buffer.write(data)
				sys.stdout.flush()

			
		print ("Exiting Receiver")

class SendThread (threading.Thread):
	def __init__(self, socket, keyEv, queue):
		threading.Thread.__init__(self)
		self.socket = socket
		self.keyEv = keyEv
		self.queue = queue
		self.stop = False

	def run(self):
		print ("Starting Sender")
		while not self.stop:
			if self.keyEv.wait(1):
				while not self.queue.empty():
					self.socket.send(bytes(self.queue.get(), 'utf-8'))
				self.keyEv.clear()

		print ("Exiting Sender")

def connectToDevice(ipaddress):
	print("Connecting ...")

	# Create a TCP/IP socket
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

	# Connect the socket to the port where the server is listening
	server_address = (ipaddress, 11234)
	print('connecting to {} port {}'.format(*server_address))
	sock.connect(server_address)

	ev_KeyPress = threading.Event()
	keyQueue = queue.Queue(128)

	inputThread = InputThread(ev_KeyPress, keyQueue)
	recvThread = ReceiverThread(sock)
	sendThread = SendThread(sock, ev_KeyPress, keyQueue)

	recvThread.start()
	sendThread.start()
	inputThread.start()

	inputThread.join()

	recvThread.stop = True
	sendThread.stop = True
	ev_KeyPress.set()

	recvThread.join()
	sendThread.join()

def listDevices(timeout):
	print("* Device name, Device Ip Address")
	sock = socket.socket(socket.AF_INET, # Internet
	                     socket.SOCK_DGRAM) # UDP

	sock.bind(("0.0.0.0", 11234))

	knownDevices = {}

	startTime = time.time()

	while True:
		ready = select.select([sock], [], [], 1)
		if ready[0]:
			data, addr = sock.recvfrom(1024)
			ip = addr[0]
			if not ip in knownDevices:
				knownDevices[ip] = data[1:]
				print(str(data[1:],'utf-8'), ',', ip)

		if timeout > 0 and (time.time() - startTime > timeout):
			break


parser = argparse.ArgumentParser()

parser.add_argument("-c", "--connect", help="connect to <ip>", action="store")
parser.add_argument("-l", "--list", help="Search for devices", nargs='?', const='30', default=False)

args = parser.parse_args()

if args.connect:
	connectToDevice(args.connect)
if args.list:
	listDevices(float(args.list))


print (args)



